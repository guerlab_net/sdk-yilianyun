package net.guerlab.sdk.yilianyun;

import org.junit.Test;

import net.guerlab.sdk.yilianyun.request.OrderAutoPrintRequest;
import net.guerlab.sdk.yilianyun.request.OrderBtnPrintRequest;
import net.guerlab.sdk.yilianyun.request.PrinterAcceptOrderRequest;
import net.guerlab.sdk.yilianyun.request.PrinterAddRequest;
import net.guerlab.sdk.yilianyun.request.PrinterDeleteRequest;
import net.guerlab.sdk.yilianyun.request.PrinterRejectOrderRequest;

/**
 * printer demo
 * 
 * @author guer
 *
 */
public class PrinterDemo {

    /**
     * 添加
     */
    @Test
    public void add() {
        PrinterAddRequest request = new PrinterAddRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);
        request.setMsign(DemoConfig.MSIGN);

        DemoConfig.CLIENT.execute(request).getData();
    }

    /**
     * 删除
     */
    @Test
    public void delete() {
        PrinterDeleteRequest request = new PrinterDeleteRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        DemoConfig.CLIENT.execute(request).getData();
    }

    /**
     * 打印机自动打印设置
     */
    @Test
    public void autoPrint() {
        OrderAutoPrintRequest request = new OrderAutoPrintRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        System.out.println(DemoConfig.CLIENT.execute(request).getData());
    }

    /**
     * 打印机按键打印设置
     */
    @Test
    public void btnPrint() {
        OrderBtnPrintRequest request = new OrderBtnPrintRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        System.out.println(DemoConfig.CLIENT.execute(request).getData());
    }

    /**
     * 打印机接单设置
     */
    @Test
    public void acceptOrder() {
        PrinterAcceptOrderRequest request = new PrinterAcceptOrderRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        System.out.println(DemoConfig.CLIENT.execute(request).getData());
    }

    /**
     * 打印机接单设置
     */
    @Test
    public void rejectOrder() {
        PrinterRejectOrderRequest request = new PrinterRejectOrderRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        System.out.println(DemoConfig.CLIENT.execute(request).getData());
    }
}
