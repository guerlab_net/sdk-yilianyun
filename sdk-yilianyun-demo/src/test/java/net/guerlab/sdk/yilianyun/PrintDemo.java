package net.guerlab.sdk.yilianyun;

import org.junit.Test;

import net.guerlab.sdk.yilianyun.entity.PrintInfo;
import net.guerlab.sdk.yilianyun.request.CancelallRequest;
import net.guerlab.sdk.yilianyun.request.CanceloneRequest;
import net.guerlab.sdk.yilianyun.request.PrintRequest;

/**
 * printer demo
 * 
 * @author guer
 *
 */
public class PrintDemo {

    /**
     * 添加
     */
    @Test
    public void print() {
        StringBuilder builder = new StringBuilder();
        builder.append("<MS>1,2</MS>");
        builder.append("<MC>4,00010,2</MC>");
        builder.append("<FB>");
        builder.append("<center>*****<FS2>共享点餐</FS2>*****</center>");
        builder.append("\r\n");
        builder.append("<center>财务饺子王(经典广场店)</center>");
        builder.append("\r\n");
        builder.append("<FS2><center>- 已在线支付 -</center></FS2>");
        builder.append("\r\n");
        builder.append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        builder.append("\r\n");
        builder.append("下单时间:");
        builder.append("2017-10-01 12:21:56");
        builder.append("\r\n");

        builder.append("<FH>");
        builder.append("<table>");
        builder.append("<tr>");
        builder.append("<td>辣椒酱</td>");
        builder.append("<td>X1</td>");
        builder.append("<td>0.0</td>");
        builder.append("</tr>");
        builder.append("<tr>");
        builder.append("<td>(煎)玉米肉馅饺</td>");
        builder.append("<td>X1</td>");
        builder.append("<td>15.98</td>");
        builder.append("</tr>");
        builder.append("<tr>");
        builder.append("<td>饺子伴侣.1分饺子限购任意酱料共2份.</td>");
        builder.append("<td>X1</td>");
        builder.append("<td>0.0</td>");
        builder.append("</tr>");
        builder.append("</table>");
        builder.append("</FH>");

        builder.append("~~~~~~~~~~其他费用~~~~~~~~~~~");
        builder.append("\r\n");

        builder.append("<FH>");
        builder.append("<table>");
        builder.append("<tr>");
        builder.append("<td>餐盒</td>");
        builder.append("<td>X1</td>");
        builder.append("<td>4.0</td>");
        builder.append("</tr>");
        builder.append("<tr>");
        builder.append("<td>在线支付立减优惠</td>");
        builder.append("<td></td>");
        builder.append("<td>-24.0</td>");
        builder.append("</tr>");
        builder.append("<tr>");
        builder.append("<td>红包</td>");
        builder.append("<td></td>");
        builder.append("<td>-2.0</td>");
        builder.append("</tr>");
        builder.append("<tr>");
        builder.append("<td>配送费</td>");
        builder.append("<td></td>");
        builder.append("<td>2.5</td>");
        builder.append("</tr>");
        builder.append("</table>");
        builder.append("</FH>");

        builder.append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        builder.append("\r\n");

        builder.append("<FS2>");
        builder.append("<table>");
        builder.append("<tr>");
        builder.append("<td>已付</td>");
        builder.append("<td></td>");
        builder.append("<td>10.46</td>");
        builder.append("</tr>");
        builder.append("</table>");
        builder.append("</FS2>");

        builder.append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        builder.append("\r\n");

        builder.append("<FS2>");
        builder.append("财富大厦B财富村经典地铁站A出口811");
        builder.append("\r\n");
        builder.append("周先生");
        builder.append("\r\n");
        builder.append("13800138000");
        builder.append("</FS2>");

        builder.append("\r\n");
        builder.append("\r\n");

        builder.append("<center>订单号:20171113110305123</center>");
        builder.append("\r\n");

        builder.append("<QR>http://www.gongxiangdiancan.com</QR>");

        builder.append("\r\n");

        builder.append("<center>*****<FS2>共享点餐</FS2>*****</center>");

        builder.append("</FB>");

        PrintRequest request = new PrintRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);
        request.setOriginId(System.currentTimeMillis() + "");
        request.setContent(builder.toString());

        PrintInfo printInfo = DemoConfig.CLIENT.execute(request).getData();

        System.out.println(printInfo);
    }

    @Test
    public void cancelone() {
        CanceloneRequest request = new CanceloneRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);
        request.setOrderId("68906044");

        DemoConfig.CLIENT.execute(request);
    }

    @Test
    public void cancelall() {
        CancelallRequest request = new CancelallRequest();
        request.setMachineCode(DemoConfig.MACHINE_CODE);

        DemoConfig.CLIENT.execute(request);
    }
}
