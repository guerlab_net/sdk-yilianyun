package net.guerlab.sdk.yilianyun;

import org.junit.Test;

import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenDTO;
import net.guerlab.sdk.yilianyun.request.AccessTokenRequest;

/**
 * AccessToken demo
 * 
 * @author guer
 *
 */
public class AccessTokenDemo {

    /**
     * 获取access token
     */
    @Test
    public void accessToken() {

        AccessTokenRequest request = new AccessTokenRequest();

        AccessTokenDTO accessTokenDTO = DemoConfig.CLIENT.execute(request).getData();

        System.out.println(accessTokenDTO);
    }
}
