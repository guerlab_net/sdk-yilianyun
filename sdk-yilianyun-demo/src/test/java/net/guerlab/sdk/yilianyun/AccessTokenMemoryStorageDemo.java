package net.guerlab.sdk.yilianyun;

import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenDTO;
import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenStorage;

/**
 * AccessToken内存存储实例
 * 
 * @author guer
 *
 */
public class AccessTokenMemoryStorageDemo implements AccessTokenStorage {

    private AccessTokenDTO accessTokenDTO;

    @Override
    public void put(
            AccessTokenDTO accessTokenDTO) {
        if (accessTokenDTO == null) {
            return;
        }

        this.accessTokenDTO = accessTokenDTO;
    }

    @Override
    public AccessTokenDTO get() {
        return accessTokenDTO;
    }

}
