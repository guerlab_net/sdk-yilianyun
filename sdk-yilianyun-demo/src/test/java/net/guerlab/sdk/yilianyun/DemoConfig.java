package net.guerlab.sdk.yilianyun;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenDTO;
import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenManager;
import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenStorage;
import net.guerlab.sdk.yilianyun.client.YiLianYunClient;
import net.guerlab.sdk.yilianyun.client.impl.DefaultYiLianYunClient;
import net.guerlab.sdk.yilianyun.config.YiLianYunAutoConfiguration;

/**
 * demo配置
 * 
 * @author guer
 *
 */
public class DemoConfig {

    /**
     * 应用id
     */
    public static final String APP_ID = "1034566678";

    /**
     * 应用密钥
     */
    public static final String SECRET_KEY = "6e2a3b506c66ce43fc9819f6d64acc3f";

    public static final String MACHINE_CODE = "4004542401";

    public static final String MSIGN = "p4qi3xwyv72y";

    public static final String ACCESS_TOKEN = "685224a4a06842e3bf80eff79e2ecd16";

    /**
     * 请求客户端对象
     */
    public static final YiLianYunClient CLIENT = new DefaultYiLianYunClient(APP_ID, SECRET_KEY,
            YiLianYunAutoConfiguration.createHttpClient(), new ObjectMapper());

    static {
        AccessTokenManager manager = AccessTokenManager.instance();
        manager.setClient(CLIENT);

        AccessTokenStorage storage = new AccessTokenMemoryStorageDemo();

        AccessTokenDTO accessTokenDTO = new AccessTokenDTO();

        accessTokenDTO.setAccessToken(ACCESS_TOKEN);
        accessTokenDTO.setExpiresIn(2592000L);
        accessTokenDTO.setExpiresInTo(2592000L + System.currentTimeMillis());

        storage.put(accessTokenDTO);

        manager.setStorage(storage);
    }
}
