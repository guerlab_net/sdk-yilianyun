package net.guerlab.sdk.yilianyun.response;

/**
 * 打印机拒单设置响应
 * 
 * @author guer
 *
 */
public class PrinterRejectOrderResponse extends AbstractResponse<String> {

}
