package net.guerlab.sdk.yilianyun.response;

/**
 * 取消单条未打印订单响应
 * 
 * @author guer
 *
 */
public class CanceloneResponse extends AbstractResponse<Void> {

}
