package net.guerlab.sdk.yilianyun.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 抽象响应对象
 * 
 * @author guer
 *
 * @param <T>
 *            响应数据类型
 */
public abstract class AbstractResponse<T> {

    /**
     * 错误码
     */
    private int error;

    /**
     * 错误说明
     */
    @JsonProperty("error_description")
    private String errorDescription;

    /**
     * 响应数据
     */
    @JsonProperty("body")
    private T data;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AbstractResponse [error=");
        builder.append(error);
        builder.append(", errorDescription=");
        builder.append(errorDescription);
        builder.append(", data=");
        builder.append(data);
        builder.append("]");
        return builder.toString();
    }

    /**
     * 返回 错误码
     *
     * @return 错误码
     */
    public int getError() {
        return error;
    }

    /**
     * 设置错误码
     *
     * @param error
     *            错误码
     */
    public void setError(
            int error) {
        this.error = error;
    }

    /**
     * 返回 错误说明
     *
     * @return 错误说明
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * 设置错误说明
     *
     * @param errorDescription
     *            错误说明
     */
    public void setErrorDescription(
            String errorDescription) {
        this.errorDescription = errorDescription;
    }

    /**
     * 返回 响应数据
     *
     * @return 响应数据
     */
    public T getData() {
        return data;
    }

    /**
     * 设置响应数据
     *
     * @param data
     *            响应数据
     */
    public void setData(
            T data) {
        this.data = data;
    }
}
