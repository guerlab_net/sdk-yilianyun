package net.guerlab.sdk.yilianyun.accesstoken;

import org.apache.commons.lang3.StringUtils;

/**
 * AccessToken储存接口
 * 
 * @author guer
 *
 */
public interface AccessTokenStorage {

    /**
     * 保存accessTokenDTO
     * 
     * @param accessTokenDTO
     *            accessTokenDTO
     */
    void put(
            AccessTokenDTO accessTokenDTO);

    /**
     * 获取accessTokenDTO
     * 
     * @return accessTokenDTO
     */
    AccessTokenDTO get();

    /**
     * 获取accessToken
     * 
     * @return accessToken
     */
    default String getAccessToken() {
        AccessTokenDTO accessTokenDTO = get();

        if (accessTokenDTO == null) {
            return null;
        }

        if (accessTokenDTO.getExpiresIn() == null || StringUtils.isBlank(accessTokenDTO.getAccessToken())) {
            return null;
        }

        long now = System.currentTimeMillis();

        if (now > accessTokenDTO.getExpiresInTo()) {
            return null;
        }

        return accessTokenDTO.getAccessToken();
    }
}
