package net.guerlab.sdk.yilianyun.accesstoken;

/**
 * AccessToken内存存储实例
 * 
 * @author guer
 *
 */
class AccessTokenMemoryStorage implements AccessTokenStorage {

    private AccessTokenDTO accessTokenDTO;

    @Override
    public void put(
            AccessTokenDTO accessTokenDTO) {
        if (accessTokenDTO == null) {
            return;
        }

        this.accessTokenDTO = accessTokenDTO;
    }

    @Override
    public AccessTokenDTO get() {
        return accessTokenDTO;
    }

}
