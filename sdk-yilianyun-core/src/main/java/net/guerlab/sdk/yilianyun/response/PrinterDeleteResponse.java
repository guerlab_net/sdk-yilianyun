package net.guerlab.sdk.yilianyun.response;

/**
 * 删除打印机响应
 * 
 * @author guer
 *
 */
public class PrinterDeleteResponse extends AbstractResponse<Void> {

}
