package net.guerlab.sdk.yilianyun.request;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenDTO;
import net.guerlab.sdk.yilianyun.response.AccessTokenResponse;
import okhttp3.FormBody.Builder;

/**
 * AccessToken请求
 * 
 * @author guer
 *
 */
public class AccessTokenRequest extends AbstractRequest<AccessTokenResponse> {

    @Override
    public void createRequestUri0(
            StringBuilder builder) {
        builder.append(YiLianYunConstants.URL_GET_ACCESS_TOKEN);
    }

    @Override
    protected void createRequestBody0(
            Builder builder) {
        builder.add("grant_type", "client_credentials");
        builder.add("scope", "all");
    }

    @Override
    protected void parseResponse0(
            String responseData) {
        super.parseResponse0(responseData);

        AccessTokenDTO accessTokenDTO = response.getData();

        accessTokenDTO.setExpiresInTo(accessTokenDTO.getExpiresIn() * 1000 + System.currentTimeMillis());
    }

}
