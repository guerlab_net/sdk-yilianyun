package net.guerlab.sdk.yilianyun.client;

import net.guerlab.sdk.yilianyun.request.AbstractRequest;
import net.guerlab.sdk.yilianyun.response.AbstractResponse;

/**
 * 蜂鸟开放平台请求客户端接口
 * 
 * @author guer
 *
 */
public interface YiLianYunClient {

    /**
     * 执行请求
     * 
     * @param request
     *            请求
     * @param <T>
     *            请求实体类型
     * @return 响应
     */
    <T extends AbstractResponse<?>> T execute(
            AbstractRequest<T> request);
}
