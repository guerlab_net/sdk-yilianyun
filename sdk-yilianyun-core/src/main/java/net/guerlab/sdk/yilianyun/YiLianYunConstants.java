package net.guerlab.sdk.yilianyun;

/**
 * 常量
 * 
 * @author guer
 *
 */
public abstract class YiLianYunConstants {

    /**
     * UTF-8编码格式
     */
    public static final String CHARSET_UTF8 = "UTF-8";

    /**
     * 基础URL
     */
    // public static final String BASE_URL = "http://127.0.0.1:8080/";
    public static final String BASE_URL = "https://open-api.10ss.net/";

    /**
     * 成功响应码
     */
    public static final int CODE_SUCCESS = 0;

    /**
     * 获取accessToken
     */
    public static final String URL_GET_ACCESS_TOKEN = "oauth/oauth";

    /**
     * 添加打印机
     */
    public static final String URL_PRINTER_ADD = "printer/addprinter";

    /**
     * 删除打印机
     */
    public static final String URL_PRINTER_DELETE = "printer/deleteprinter";

    /**
     * 接单/拒单
     */
    public static final String URL_PRINTER_GETORDER = "printer/getorder";

    /**
     * 打印方式
     */
    public static final String URL_PRINTER_BTNPRINT = "printer/btnprint";

    /**
     * 打印
     */
    public static final String URL_PRINT = "print/index";

    /**
     * 取消单条未打印订单
     */
    public static final String URL_CANCEL_ONE = "printer/cancelone";

    /**
     * 取消所有未打印订单
     */
    public static final String URL_CANCEL_ALL = "printer/cancelall";

    private YiLianYunConstants() {
        /**
         * not to do some thing
         */
    }

}
