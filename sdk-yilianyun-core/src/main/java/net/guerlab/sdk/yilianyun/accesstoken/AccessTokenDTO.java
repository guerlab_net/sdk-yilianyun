package net.guerlab.sdk.yilianyun.accesstoken;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * AccessToken
 * 
 * @author guer
 *
 */
public class AccessTokenDTO {

    /**
     * access_token
     */
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * refresh_token
     */
    @JsonProperty("refresh_token")
    private String refreshToken;

    /**
     * 超时时间
     */
    @JsonProperty("expires_in")
    private Long expiresIn;

    /**
     * 超时时间点
     */
    @JsonProperty("expires_in_to")
    private Long expiresInTo;

    /**
     * scope
     */
    private String scope;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccessTokenDTO [accessToken=");
        builder.append(accessToken);
        builder.append(", refreshToken=");
        builder.append(refreshToken);
        builder.append(", expiresIn=");
        builder.append(expiresIn);
        builder.append(", scope=");
        builder.append(scope);
        builder.append("]");
        return builder.toString();
    }

    /**
     * 返回 accessToken
     *
     * @return accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置accessToken
     *
     * @param accessToken
     *            accessToken
     */
    public void setAccessToken(
            String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 返回 refreshToken
     *
     * @return refreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 设置refreshToken
     *
     * @param refreshToken
     *            refreshToken
     */
    public void setRefreshToken(
            String refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * 返回 超时时间
     *
     * @return 超时时间
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * 设置超时时间
     *
     * @param expiresIn
     *            超时时间
     */
    public void setExpiresIn(
            Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * 返回 scope
     *
     * @return scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * 设置scope
     *
     * @param scope
     *            scope
     */
    public void setScope(
            String scope) {
        this.scope = scope;
    }

    /**
     * 返回 超时时间点
     *
     * @return 超时时间点
     */
    public Long getExpiresInTo() {
        return expiresInTo;
    }

    /**
     * 设置超时时间点
     *
     * @param expiresInTo
     *            超时时间点
     */
    public void setExpiresInTo(
            Long expiresInTo) {
        this.expiresInTo = expiresInTo;
    }
}
