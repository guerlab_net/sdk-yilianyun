package net.guerlab.sdk.yilianyun.response;

/**
 * 取消所有未打印订单响应
 * 
 * @author guer
 *
 */
public class CancelallResponse extends AbstractResponse<Void> {

}
