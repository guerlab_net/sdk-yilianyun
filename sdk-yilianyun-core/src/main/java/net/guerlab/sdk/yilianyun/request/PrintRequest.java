package net.guerlab.sdk.yilianyun.request;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.response.PrintResponse;
import okhttp3.FormBody.Builder;

/**
 * 打印请求
 * 
 * @author guer
 *
 */
public class PrintRequest extends AbstractRequest<PrintResponse> {

    /**
     * 打印机终端号
     */
    private String machineCode;

    /**
     * 打印内容(需要urlencode)
     */
    private String content;

    /**
     * 商户系统内部订单号
     */
    private String originId;

    @Override
    protected void createRequestUri0(
            StringBuilder builder) {
        builder.append(YiLianYunConstants.URL_PRINT);
    }

    @Override
    protected void createRequestBody0(
            Builder builder) {
        builder.add("machine_code", machineCode);
        builder.add("content", content);
        builder.add("origin_id", originId);
    }

    /**
     * 返回 打印机终端号
     *
     * @return 打印机终端号
     */
    public String getMachineCode() {
        return machineCode;
    }

    /**
     * 设置打印机终端号
     *
     * @param machineCode
     *            打印机终端号
     */
    public void setMachineCode(
            String machineCode) {
        this.machineCode = machineCode;
    }

    /**
     * 返回 打印内容(需要urlencode)
     *
     * @return 打印内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置打印内容(需要urlencode)
     *
     * @param content
     *            打印内容
     */
    public void setContent(
            String content) {
        this.content = content;
    }

    /**
     * 返回 商户系统内部订单号<br>
     * 要求32个字符内，只能是数字、大小写字母 ，且在同一个client_id下唯一
     *
     * @return 商户系统内部订单号
     */
    public String getOriginId() {
        return originId;
    }

    /**
     * 设置商户系统内部订单号<br>
     * 要求32个字符内，只能是数字、大小写字母 ，且在同一个client_id下唯一
     *
     * @param originId
     *            商户系统内部订单号
     */
    public void setOriginId(
            String originId) {
        this.originId = originId;
    }

}
