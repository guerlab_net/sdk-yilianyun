package net.guerlab.sdk.yilianyun.request;

import java.lang.reflect.ParameterizedType;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.YiLianYunException;
import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenManager;
import net.guerlab.sdk.yilianyun.helper.SignHelper;
import net.guerlab.sdk.yilianyun.response.AbstractResponse;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * 抽象请求
 * 
 * @author guer
 *
 * @param <T>
 *            响应类型
 */
public abstract class AbstractRequest<T extends AbstractResponse<?>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRequest.class);

    public static final String SIGNATURE_KEY_NAME = "signature";

    private ObjectMapper objectMapper;

    /**
     * 响应体
     */
    protected T response;

    private String clientId;

    private String clientSecret;

    /**
     * 获取响应对象类型
     * 
     * @return 响应对象类型
     */
    @SuppressWarnings("unchecked")
    public Class<T> getResponseClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * 构造请求URI
     * 
     * @param builder
     *            请求URI构造器
     * @param clientId
     *            appId
     * @param clientSecret
     *            secretKey
     */
    public final void createRequestUri(
            StringBuilder builder,
            String clientId,
            String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        createRequestUri0(builder);
    }

    protected abstract void createRequestUri0(
            StringBuilder builder);

    /**
     * 执行请求
     * 
     * @param responseData
     *            响应数据
     */
    protected void parseResponse0(
            String responseData) {
        try {
            response = objectMapper.readValue(responseData, getResponseClass());
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }

        if (response == null) {
            throw new YiLianYunException("response body is empty");
        }

        if (YiLianYunConstants.CODE_SUCCESS != response.getError()) {
            throw new YiLianYunException(
                    "request fail[code=" + response.getError() + ", msg=" + response.getErrorDescription() + "]");
        }
    }

    /**
     * 获取请求内容
     * 
     * @return 请求内容
     */
    public final RequestBody createRequestBody() {
        FormBody.Builder builder = new FormBody.Builder();

        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        String sign = SignHelper.sign(clientId, clientSecret, timestamp);

        builder.add("client_id", clientId);
        builder.add("sign", sign);
        builder.add("timestamp", timestamp);
        builder.add("id", UUID.randomUUID().toString());

        if (!AccessTokenRequest.class.isAssignableFrom(getClass())) {
            builder.add("access_token", AccessTokenManager.instance().getAccessToken());
        }

        createRequestBody0(builder);

        return builder.build();
    }

    /**
     * 窗秋请求内容
     * 
     * @param builder
     *            构造器
     */
    protected abstract void createRequestBody0(
            FormBody.Builder builder);

    /**
     * 执行请求
     * 
     * @param responseData
     *            响应数据
     * @return 响应实体
     */
    public AbstractRequest<T> parseResponse(
            String responseData) {
        parseResponse0(responseData);
        return this;
    }

    /**
     * 获取响应实体
     * 
     * @return 响应实体
     */
    public final T getResponse() {
        return response;
    }

    /**
     * 返回 objectMapper
     *
     * @return objectMapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * 设置objectMapper
     *
     * @param objectMapper
     *            objectMapper
     */
    public void setObjectMapper(
            ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
