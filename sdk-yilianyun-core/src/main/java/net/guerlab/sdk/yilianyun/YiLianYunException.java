package net.guerlab.sdk.yilianyun;

/**
 * 易联云异常
 * 
 * @author guer
 *
 */
public class YiLianYunException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 通过异常信息构造异常
     * 
     * @param message
     *            异常信息
     */
    public YiLianYunException(
            String message) {
        super(message);
    }

    /**
     * 通过异常信息和源异常构造异常
     * 
     * @param message
     *            异常信息
     * @param e
     *            源异常
     */
    public YiLianYunException(
            String message,
            Exception e) {
        super(message, e);
    }
}
