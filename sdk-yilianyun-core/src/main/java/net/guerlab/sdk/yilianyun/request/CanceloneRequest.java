package net.guerlab.sdk.yilianyun.request;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.response.CanceloneResponse;
import okhttp3.FormBody.Builder;

/**
 * 取消所有未打印订单请求
 * 
 * @author guer
 *
 */
public class CanceloneRequest extends AbstractRequest<CanceloneResponse> {

    /**
     * 打印机终端号
     */
    private String machineCode;

    /**
     * 打印订单号
     */
    private String orderId;

    @Override
    protected void createRequestUri0(
            StringBuilder builder) {
        builder.append(YiLianYunConstants.URL_CANCEL_ONE);
    }

    @Override
    protected void createRequestBody0(
            Builder builder) {
        builder.add("machine_code", machineCode);
        builder.add("order_id", orderId);
    }

    /**
     * 返回 打印机终端号
     *
     * @return 打印机终端号
     */
    public String getMachineCode() {
        return machineCode;
    }

    /**
     * 设置打印机终端号
     *
     * @param machineCode
     *            打印机终端号
     */
    public void setMachineCode(
            String machineCode) {
        this.machineCode = machineCode;
    }

    /**
     * 返回 打印订单号
     *
     * @return 打印订单号
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置打印订单号
     *
     * @param orderId
     *            打印订单号
     */
    public void setOrderId(
            String orderId) {
        this.orderId = orderId;
    }

}