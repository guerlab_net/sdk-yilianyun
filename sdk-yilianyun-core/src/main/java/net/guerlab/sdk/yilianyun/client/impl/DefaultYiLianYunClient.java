package net.guerlab.sdk.yilianyun.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.YiLianYunException;
import net.guerlab.sdk.yilianyun.client.AbstractYiLianYunClient;
import net.guerlab.sdk.yilianyun.request.AbstractRequest;
import net.guerlab.sdk.yilianyun.response.AbstractResponse;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

/**
 * 默认易连云请求客户端
 *
 * @author guer
 *
 */
public class DefaultYiLianYunClient extends AbstractYiLianYunClient {

    private OkHttpClient okHttpClient;

    private ObjectMapper objectMapper;

    /**
     * 构造易联云开放平台请求客户端
     *
     * @param clientId
     *            clientId
     * @param clientSecret
     *            clientSecret
     * @param okHttpClient
     *            http请求客户端
     * @param objectMapper
     *            objectMapper
     */
    public DefaultYiLianYunClient(String clientId, String clientSecret, OkHttpClient okHttpClient,
            ObjectMapper objectMapper) {
        super(clientId, clientSecret);
        this.okHttpClient = okHttpClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public <T extends AbstractResponse<?>> T execute(
            AbstractRequest<T> request) {
        StringBuilder builder = new StringBuilder(YiLianYunConstants.BASE_URL);

        request.setObjectMapper(objectMapper);

        request.createRequestUri(builder, clientId, clientSecret);

        return execute0(request, builder.toString());
    }

    private <T extends AbstractResponse<?>> T execute0(
            AbstractRequest<T> request,
            String uri) {
        return executeWithHttpRequest(request, uri);
    }

    private <T extends AbstractResponse<?>> T executeWithHttpRequest(
            AbstractRequest<T> request,
            String uri) {
        Builder builder = new Request.Builder();
        builder.url(uri);

        builder.post(request.createRequestBody());

        return request.parseResponse(getHttpResponceString(builder)).getResponse();
    }

    private String getHttpResponceString(
            Builder builder) {
        Call call = okHttpClient.newCall(builder.build());
        try {
            Response response = call.execute();
            return response.body().string();
        } catch (Exception e) {
            throw new YiLianYunException(e.getMessage(), e);
        }
    }
}
