package net.guerlab.sdk.yilianyun.helper;

import java.net.URLDecoder;
import java.net.URLEncoder;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.YiLianYunException;

/**
 * URL编码解码助手
 * 
 * @author guer
 *
 */
public class URLHelper {

    private URLHelper() {
    }

    /**
     * 编码
     * 
     * @param source
     *            原内容
     * @return 编码内容
     */
    public static String encode(
            String source) {
        try {
            return URLEncoder.encode(source, YiLianYunConstants.CHARSET_UTF8);
        } catch (Exception e) {
            throw new YiLianYunException(e.getMessage(), e);
        }
    }

    /**
     * 解码
     * 
     * @param source
     *            原内容
     * @return 解码内容
     */
    public static String decode(
            String source) {
        try {
            return URLDecoder.decode(source, YiLianYunConstants.CHARSET_UTF8);
        } catch (Exception e) {
            throw new YiLianYunException(e.getMessage(), e);
        }
    }
}
