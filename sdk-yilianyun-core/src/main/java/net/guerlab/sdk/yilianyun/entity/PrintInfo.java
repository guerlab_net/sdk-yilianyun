package net.guerlab.sdk.yilianyun.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 打印信息
 * 
 * @author guer
 *
 */
public class PrintInfo {

    /**
     * 打印机订单id
     */
    private String id;

    /**
     * 商户系统内部订单号<br>
     * 要求32个字符内，只能是数字、大小写字母 ，且在同一个client_id下唯一
     */
    @JsonProperty("origin_id")
    private String originId;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PrintInfo [id=");
        builder.append(id);
        builder.append(", originId=");
        builder.append(originId);
        builder.append("]");
        return builder.toString();
    }

    /**
     * 返回 打印机订单id
     *
     * @return 打印机订单id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置打印机订单id
     *
     * @param id
     *            打印机订单id
     */
    public void setId(
            String id) {
        this.id = id;
    }

    /**
     * 返回 商户系统内部订单号<br>
     * 要求32个字符内，只能是数字、大小写字母 ，且在同一个client_id下唯一
     *
     * @return 商户系统内部订单号
     */
    public String getOriginId() {
        return originId;
    }

    /**
     * 设置商户系统内部订单号<br>
     * 要求32个字符内，只能是数字、大小写字母 ，且在同一个client_id下唯一
     *
     * @param originId
     *            商户系统内部订单号
     */
    public void setOriginId(
            String originId) {
        this.originId = originId;
    }
}
