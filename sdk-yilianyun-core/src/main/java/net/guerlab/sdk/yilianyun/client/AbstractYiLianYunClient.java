package net.guerlab.sdk.yilianyun.client;

/**
 * 抽象易联云开放平台请求客户端
 * 
 * @author guer
 *
 */
public abstract class AbstractYiLianYunClient implements YiLianYunClient {

    /**
     * appId
     */
    protected String clientId;

    /**
     * secretKey
     */
    protected String clientSecret;

    /**
     * 构造易联云开放平台请求客户端
     * 
     * @param clientId
     *            clientId
     * @param clientSecret
     *            clientSecret
     */
    public AbstractYiLianYunClient(
            String clientId,
            String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }
}
