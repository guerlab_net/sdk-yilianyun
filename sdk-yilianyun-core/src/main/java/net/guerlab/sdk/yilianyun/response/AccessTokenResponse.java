package net.guerlab.sdk.yilianyun.response;

import net.guerlab.sdk.yilianyun.accesstoken.AccessTokenDTO;

/**
 * access token请求响应
 * 
 * @author guer
 *
 */
public class AccessTokenResponse extends AbstractResponse<AccessTokenDTO> {

}
