package net.guerlab.sdk.yilianyun.request;

import org.apache.commons.lang3.StringUtils;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.response.PrinterAddResponse;
import okhttp3.FormBody.Builder;

/**
 * 添加打印机请求
 * 
 * @author guer
 *
 */
public class PrinterAddRequest extends AbstractRequest<PrinterAddResponse> {

    /**
     * 打印机终端号
     */
    private String machineCode;

    /**
     * 终端密钥
     */
    private String msign;

    /**
     * 手机卡号码
     */
    private String phone;

    /**
     * 自定义打印机名称
     */
    private String printName;

    @Override
    protected void createRequestUri0(
            StringBuilder builder) {
        builder.append(YiLianYunConstants.URL_PRINTER_ADD);
    }

    @Override
    protected void createRequestBody0(
            Builder builder) {
        builder.add("machine_code", machineCode);
        builder.add("msign", msign);

        if (StringUtils.isNotBlank(phone)) {
            builder.add("phone", phone);
        }

        if (StringUtils.isNotBlank(printName)) {
            builder.add("print_name", printName);
        }
    }

    /**
     * 返回 打印机终端号
     *
     * @return 打印机终端号
     */
    public String getMachineCode() {
        return machineCode;
    }

    /**
     * 设置打印机终端号
     *
     * @param machineCode
     *            打印机终端号
     */
    public void setMachineCode(
            String machineCode) {
        this.machineCode = machineCode;
    }

    /**
     * 返回 终端密钥
     *
     * @return 终端密钥
     */
    public String getMsign() {
        return msign;
    }

    /**
     * 设置终端密钥
     *
     * @param msign
     *            终端密钥
     */
    public void setMsign(
            String msign) {
        this.msign = msign;
    }

    /**
     * 返回 手机卡号码(可填)
     *
     * @return 手机卡号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置手机卡号码(可填)
     *
     * @param phone
     *            手机卡号码
     */
    public void setPhone(
            String phone) {
        this.phone = phone;
    }

    /**
     * 返回 自定义打印机名称(可填)
     *
     * @return 自定义打印机名称
     */
    public String getPrintName() {
        return printName;
    }

    /**
     * 设置自定义打印机名称(可填)
     *
     * @param printName
     *            自定义打印机名称
     */
    public void setPrintName(
            String printName) {
        this.printName = printName;
    }

}
