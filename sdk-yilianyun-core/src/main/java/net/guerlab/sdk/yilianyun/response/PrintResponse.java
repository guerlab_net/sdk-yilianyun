package net.guerlab.sdk.yilianyun.response;

import net.guerlab.sdk.yilianyun.entity.PrintInfo;

/**
 * 打印响应
 * 
 * @author guer
 *
 */
public class PrintResponse extends AbstractResponse<PrintInfo> {

}
