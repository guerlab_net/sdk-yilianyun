package net.guerlab.sdk.yilianyun.request;

import net.guerlab.sdk.yilianyun.YiLianYunConstants;
import net.guerlab.sdk.yilianyun.response.OrderAutoPrintResponse;
import okhttp3.FormBody.Builder;

/**
 * 打印机按键打印设置请求
 * 
 * @author guer
 *
 */
public class OrderBtnPrintRequest extends AbstractRequest<OrderAutoPrintResponse> {

    /**
     * 打印机终端号
     */
    private String machineCode;

    @Override
    protected void createRequestUri0(
            StringBuilder builder) {
        builder.append(YiLianYunConstants.URL_PRINTER_BTNPRINT);
    }

    @Override
    protected void createRequestBody0(
            Builder builder) {
        builder.add("machine_code", machineCode);
        builder.add("response_type", "btnopen");
    }

    /**
     * 返回 打印机终端号
     *
     * @return 打印机终端号
     */
    public String getMachineCode() {
        return machineCode;
    }

    /**
     * 设置打印机终端号
     *
     * @param machineCode
     *            打印机终端号
     */
    public void setMachineCode(
            String machineCode) {
        this.machineCode = machineCode;
    }

}
