package net.guerlab.sdk.yilianyun.helper;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 签名助手
 * 
 * @author guer
 *
 */
public class SignHelper {

    private SignHelper() {

    }

    /**
     * 获取签名内容
     * 
     * @param clientId
     *            用户id
     * @param clientSecret
     *            API密钥
     * @param timestamp
     *            时间戳(10位)
     * @return 签名
     */
    public static String sign(
            String clientId,
            String clientSecret,
            String timestamp) {
        StringBuilder builder = new StringBuilder(clientId);
        builder.append(timestamp);
        builder.append(clientSecret);

        return DigestUtils.md5Hex(builder.toString());
    }
}
