package net.guerlab.sdk.yilianyun.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 易联云配置
 * 
 * @author guer
 *
 */
@Component
@RefreshScope
@ConfigurationProperties(prefix = YiLianYunConfig.CONFIG_PREFIX)
public class YiLianYunConfig {

    public static final String CONFIG_PREFIX = "sdk.yilianyun";

    /**
     * 应用ID
     */
    private String clientId;

    /**
     * 应用密钥
     */
    private String clientSecret;

    /**
     * 返回 应用ID
     *
     * @return 应用ID
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * 设置应用ID
     *
     * @param clientId
     *            应用ID
     */
    public void setClientId(
            String clientId) {
        this.clientId = clientId;
    }

    /**
     * 返回 应用密钥
     *
     * @return 应用密钥
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * 设置应用密钥
     *
     * @param clientSecret
     *            应用密钥
     */
    public void setClientSecret(
            String clientSecret) {
        this.clientSecret = clientSecret;
    }
}
